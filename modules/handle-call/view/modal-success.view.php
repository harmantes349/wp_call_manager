<?php
/**
 * Form view of "Call Manager" module.
 *
 * @author Eoxia <dev@eoxia.com>
 * @since 2.0.0
 * @version 2.0.0
 * @copyright 2018
 * @package call_manager
 */

namespace handle_call;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

	<h1 id="body_modal_success"><?php echo esc_html_e( 'Comment insert is Success', 'call-manager' ); ?></h1>
