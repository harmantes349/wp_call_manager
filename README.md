# Call Manager ![Eoxia Logo](https://avatars0.githubusercontent.com/u/3227847?s=200&amp;v=4)
### version 2.0.0
###### Call Manager est un plugin WordPress qui nesseciste le plugins [WP_shop](https://github.com/Eoxia/wpshop) de [Eoxia](https://github.com/eoxia) pour fonctionner correctement.

## Fonctionnalitée

* Le plugin ajoute 1 boutton supplementaire dans l'admin bar de WordPress .
* au click sur ce boutton une modal s'ouvre.
* dans cette modal il vous sera demander de selectionner l'administrateur conserner, le status de l'appel, le client Wp_shop .
* si le client Wp_shop est introuvaple il vous sera demander de click sur un boutton "ajouter nouveau client", apres un formulaire supplementaire va pop pour vous permetre de cree un nouveau clients WP_shop .
* Apres il vous reste juste à taper un commentaire consernant l'appel reçu et clické sur validé . le commentaire sera liée au clients et au administrateur conserné .

## Table

###### Après son activation le plugin ne rajoute pas de table supplementaire les seul table utilisé sont 'wp_comments','wp_postmeta','wp_posts'.
